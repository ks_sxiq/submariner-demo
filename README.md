# submariner-demo

This demo is suitable for demonstrating nginx app deployment to a managed cluster(in this repo i referred it as "managed-cluster01") via ACM HUB and also verifying multi-cluster network communication via submariner.

## Prerequisites

Deploy the Cluster specific nginx index-file config

On Managed-Cluster01:

```
oc login https://api.managed-cluster01.openshift.sxiq.net.au:6443 --username adminuser --password adminpassword
oc apply -f shared-configs/index-file-configmap-managedCluster01.yaml
```

On Managed-Cluster02:

```
oc login https://api.managed-cluster02.openshift.sxiq.net.au:6443 --username adminuser --password adminpassword
oc apply -f shared-configs/index-file-configmap-managedCluster02.yaml
```

## Deployment via ACM Hub

If you would like to deploy the nginx app to "managed-cluster01" via ACM-HUB then connect to the acm-hub cluster and run  the following commands


```
oc login https://api.acm-hub.openshift.sxiq.net.au:6443 --username adminuser --password adminpassword
oc apply -f rhacm-resources/applications.yaml
```

## Individual deployment of the app on the managed-cluster

If you would like to deploy manually on the managed-cluster01, the connect to the managed-cluster01 and run the following

```
oc login https://api.managed-cluster01.openshift.sxiq.net.au:6443 --username adminuser --password adminpassword
oc apply -f nginx/ --recursive
```
This will create nginx-namespace, nginx-deployment, service, route and service export so that the other clusters can connect to them.

## Submariner Verification tests using dns and curl

run a test pod (for example ubuntu) and check the nginx is working as expected.

```
root@ubuntu:/# nslookup nginx
Server:         172.40.0.10
Address:        172.40.0.10#53

Name:   nginx.default.svc.cluster.local
Address: 172.40.144.183
Address: 172.40.114.80

root@ubuntu:/# curl nginx:8080/
<html>
<h1>Managed-Cluster01 Nginx</h1>
</br>
<h1>Hello from Managed-Cluster01 Nginx </h1>
</html>

```
We can verify the published route via the internet on any browser by navigating to http://nginx.managed-cluster01.openshift.sxiq.net.au

```
root@ubuntu:/# curl http://nginx.managed-cluster01.openshift.sxiq.net.au
<html>
<h1>Managed-Cluster01 Nginx</h1>
</br>
<h1>Hello from Managed-Cluster01 Nginx </h1>
</html>

```

Now connect to the managed-cluster02 and run the tests using a test pod

```
root@ubuntu:/# nslookup managed-cluster01.nginx.nginx.svc.clusterset.local
Server:         172.50.0.10
Address:        172.50.0.10#53

Name:   managed-cluster01.nginx.nginx.svc.clusterset.local
Address: 172.40.114.80

root@ubuntu:/# curl managed-cluster01.nginx.nginx.svc.clusterset.local:8080
<html>
<h1>Managed-Cluster01 Nginx</h1>
</br>
<h1>Hello from Managed-Cluster01 Nginx </h1>
</html>

```


## Authors
Kumaresh Sundaramurthy/SXiQ, an IBM Company

